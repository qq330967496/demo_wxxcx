Page({
  data: {
    text:'初始化数据'
  },
  onLoad:function(){
    console.log('生命周期-页面加载');
  },
  onReady:function(){
    console.log('生命周期-初次渲染完成');
  },
  onShow:function(){
    console.log('生命周期-页面显示');
  },
  onHide:function(){
    console.log('生命周期-页面隐藏');
  },
  onUnload:function(){
    console.log('生命周期-页面卸载');
  },
  onPullDownRefresh:function(){
    console.log('页面相关事件-下拉动作');
  },
  onReachBottom:function(){
    console.log('页面相关事件-到底部');
  },
  //其他方法
  viewTap:function(){
    var _this = this;
    _this.setData({
      text:'更新后数据'
    });
  },
  getCurrentPages:function(){
    /*
    getCurrentPages() 函数用于获取当前页面栈的实例，以数组形式按栈的顺序给出，第一个元素为首页，最后一个元素为当前页面。

    ps:可以用于各个页面之间的通信。
    curPages[0].data获取首页的data

    */
    var curPages = getCurrentPages();

    console.log(curPages[0].data.text);
  }
});