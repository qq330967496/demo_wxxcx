Page({
  data: {
    title:'跳转页面'
  },
  onLoad:function(options){
    var _this = this;
    console.log('生命周期-页面加载');

    console.log('获取参数字符串：'+options.query);
    console.log('获取具体参数：'+options.title);
    _this.setData({
      title: options.title
    })
  },
  onReady:function(){
    console.log('生命周期-初次渲染完成');
  },
  //其他方法

});