Page({
  data: {
    background: ['green', 'red', 'yellow'],
    indicatorDots: true,//显示面板指示点
    vertical: false,//水平居下 or 垂直居右
    autoplay: false,//是否自动轮播
    interval: 3000,//轮播时间
    duration: 1200,//滑动时长
    current:1,//当前页面index
  },
  bindchange:function(e){
    this.setData({
      current: e.detail.current
    })
  },
  bindCurrent: function(e) {
    this.setData({
      current: e.detail.value
    })
  },
  changeIndicatorDots: function (e) {
    this.setData({
      indicatorDots: !this.data.indicatorDots
    })
  },
  changeVertical: function (e) {
    this.setData({
      vertical: !this.data.vertical
    })
  },
  changeAutoplay: function (e) {
    this.setData({
      autoplay: !this.data.autoplay
    })
  },
  intervalChange: function (e) {
    this.setData({
      interval: e.detail.value
    })
  },
  durationChange: function (e) {
    this.setData({
      duration: e.detail.value
    })
  }
})
