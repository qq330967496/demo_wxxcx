App({
  onLaunch: function () {
    console.log('生命周期函数-初始化 App Launch')
  },
  onShow: function () {
    console.log('生命周期函数-显示 App Show')
  },
  onHide: function () {
    console.log('生命周期函数-隐藏 App Hide')
  },
  globalData: {
    hasLogin: false
  }
})
  